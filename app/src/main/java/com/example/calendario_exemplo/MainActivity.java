package com.example.calendario_exemplo;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.GridView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private GridView gridView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = (GridView) findViewById(R.id.gridView1);

        ArrayList<Celula> celulasList = new ArrayList<>();
        for (int i = 0; i < 42; i++) {
            celulasList.add(new Celula(i));
        }

        gridView.setAdapter(new GridViewAdapter(this, celulasList));
    }
}