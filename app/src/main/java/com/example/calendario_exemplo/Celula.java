package com.example.calendario_exemplo;

public class Celula {

    private int celula_id;

    public Celula(int celula_id) {
        this.celula_id = celula_id;
    }

    public int getCelula_id() {
        return celula_id;
    }

    public void setCelula_id(int celula_id) {
        this.celula_id = celula_id;
    }
}
